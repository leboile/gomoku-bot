#include <iostream>
#include <sys/time.h>
#include <vector>
#include <unistd.h>
#include <limits.h>
#include <algorithm>
//TODO: FASTER, SMARTER, sort moves, formula, offensive/defensive tactics coefficient, GUI
//negascout doesnt work on some depths, also moves=backMoves can be reaplced with much more efficient function
using namespace std;
struct Tree
{
    //int value;
    int x;
    int y;
    int alpha;
    int beta;
    // vector<Tree*> children;
    // void addChild(Tree* node)
    // {
    // children.push_back(node);
    // }

    Tree(int i, int j)
    {
        //value=v;
        x=i;
        y=j;
        alpha=INT_MIN;
        beta=INT_MAX;
    }
};
struct coord
{
    static bool cmp(const coord& c1, const coord& c2)
    {
        if (c1.i<c2.i)
            return true;
        else if (c2.i<c1.i)
            return false;
        else if (c1.j<c2.j)
            return true;
        else
            return false;
    }
	static bool cmp2(const coord& c1, const coord& c2)
	{
	 return c1.weight<c2.weight;
	}
    int i;
    int j;
	int weight;
    coord(int x=-1, int y=-1):i(x), j(y),weight(0) {}
};
class Bot
{
    int counter;
    int n;
    int m;
    int k;
    int d;
    vector<coord> moves;
    vector<coord> elems;
    int*** field;
public:
    Bot(int maxI, int maxJ, int lineToWin, int depth, int*** f)
    {
        n=maxI;
        m=maxJ;
        k=lineToWin;
        d=depth;
        field=f;
    }
    void placeMove(int i, int j,bool isBot,int& maxLine1, int& maxLine2)
    {
        if (!(*field)[i][j])
        {
		    //checkCell(i,j);
			elems.push_back(coord(i,j));
            delMove(coord(i,j));
			sort(elems.begin(),elems.end(),coord::cmp);
		    if (isBot)
             (*field)[i][j]=1;
            else
             (*field)[i][j]=2;
			int maxLine=0;
            getStreaks(maxLine,2);
			maxLine2=maxLine;
			maxLine=0;
            getStreaks(maxLine,1);
			maxLine1=maxLine;
			sort(moves.begin(),moves.end(),coord::cmp2);
        }
        else
            throw 1;//CELL IS ALREADY TAKEN
    }
	void MoveMINIMAX(int i, int j)
    {
	    int maxLine1=0,maxLine2=0;
        counter=0;
        placeMove(i,j,0,maxLine1,maxLine2);
		print();
		vector<coord>::reverse_iterator it;
		//getStreaks(maxline,2);
        if (maxLine2>=k)
            throw 4;//human won
        Tree node(0,0);
        struct timeval s,e;
        gettimeofday(&s, NULL);
        if(d>n*m-(int)elems.size())
            d=n*m-(int)elems.size();
        if (d<=0)
            throw 2;//GAME OVER	
        minimax(node, 0);
        placeMove(node.x,node.y,1,maxLine1, maxLine2);
        gettimeofday(&e, NULL);
        cout<<"seconds: "<<(((e.tv_sec  - s.tv_sec) * 1000 + (e.tv_usec - s.tv_usec)/1000.0) + 0.5)/1000<<endl;
        cout<<"Count: "<<counter<<" possible moves size: "<<moves.size()<<" elems size: "<<elems.size()<<endl;
        cout<<"X: "<<node.x<<" Y: "<<node.y<<" alpha: "<<node.alpha<<" beta: "<<node.beta<<endl;
        //print field
        print();
		int bDepth=d;
		d=0;
		cout<<Estimator()<<endl;
		d=bDepth;
		//check for winner
        if (maxLine1>=k)
            throw 3;//robot won
    }
    void minimax(Tree& node, int prevDepth)
    {
        counter++;
        int depth=prevDepth+1;
        bool turn=depth%2!=0;
        vector<coord> backMoves(moves);
        vector<coord> backElems(elems);
        if (depth>d)
        {
            int v = Estimator();
			//if (turn)
             node.alpha=v;
			//else 
             node.beta=v;
            return;
        }
        // else
        // {   //TODO: maybe add open thirds estimating
            // int maxLine=0;
            // if (turn)
                // getStreaks(maxLine,2);
            // else
                // getStreaks(maxLine,1);
            // if (maxLine>=k)
            // {
			    // int v = Estimator();
                // if (turn)
                // {
			       // node.alpha=v;
                // }
                // else
                // {
				   // node.beta=v;
                // }
                // return;
            // }
        // }
		int alpha = node.alpha;
		int beta=node.beta;
		int maxLine1=0,maxLine2=0;
        int x,y;
        vector<coord>::reverse_iterator it;
        if (turn)
            for (it=backMoves.rbegin(); it!=backMoves.rend(); it++)
            {
                x=it->i;
                y=it->j;
                Tree newNode(x,y);
                newNode.alpha=node.alpha;
                newNode.beta=beta;
                placeMove(x,y,turn,maxLine1,maxLine2);
				if (maxLine1>=k)
				 {
				  newNode.beta=Estimator();
				 }
				else
                 minimax(newNode,depth);
				if (depth==1)
				{
				 cout<<"MOVE: "<<x<<" "<<y<<" "<<newNode.alpha<<" "<<newNode.beta<<endl;
				}
                if (newNode.beta>node.alpha)
                {
                    //cout<<"W?"<<endl;
                    node.alpha=newNode.beta;
                    if (depth==1)
                    {
                        cout<<"WHY?"<<endl;
                        node.x=newNode.x;
                        node.y=newNode.y;
                    }
                }
                 unplaceMove(x,y,backMoves);
                 if (node.alpha>=node.beta)
                     return;
            }
        else
            for (it=backMoves.rbegin(); it!=backMoves.rend(); it++)
            {
                x=it->i;
                y=it->j;
                Tree newNode(x,y);
                newNode.alpha=alpha;
                newNode.beta=node.beta;
                placeMove(x,y,turn,maxLine1,maxLine2);
				if (maxLine2>=k)
				 {
				  newNode.alpha=Estimator();
				 }
				else
                 minimax(newNode,depth);
                if (newNode.alpha<node.beta)
                    node.beta=newNode.alpha;
				unplaceMove(x,y,backMoves);
                if (node.alpha>=node.beta)
                    return;
            }
    }
	inline void unplaceMove(int x, int y,vector<coord>& backMoves)
	{
	            moves.clear();
                moves = backMoves;//set back array of possible moves
                //elems.clear();
				vector<coord>::iterator it;
                for(it=elems.begin(); it!=elems.end(); it++)
                 if (it->i==x && it->j==y)
                 {
                  elems.erase(it);
                  break;
                 }
                (*field)[x][y]=0;
	}

    inline void checkCell(int i, int j)
    {
        if(i>0 && !(*field)[i-1][j])
            addMove(coord(i-1,j));
        if (j>0 && i>0 && !(*field)[i-1][j-1])
            addMove(coord(i-1,j-1));
        if(j<m-1 && i>0 && !(*field)[i-1][j+1])
            addMove(coord(i-1,j+1));
        if(i<n-1 && !(*field)[i+1][j])
            addMove(coord(i+1,j));
        if (j>0 && i<n-1 && !(*field)[i+1][j-1])
            addMove(coord(i+1,j-1));
        if(j<m-1 && i<n-1 && !(*field)[i+1][j+1])
            addMove(coord(i+1,j+1));
        if(j>0 && !(*field)[i][j-1])
            addMove(coord(i,j-1));
        if (j<m-1 && !(*field)[i][j+1])
            addMove(coord(i,j+1));
    }
    void addMove(coord c)//make it without sorting
    {
	    bool add=true;
        vector<coord>::iterator it;
        for(it=moves.begin(); it<moves.end(); it++)
		{
            if (it->i==c.i && it->j==c.j)
			{
			    if (c.weight>it->weight)
				{
			     moves.erase(it);	
				 add=true;
                }		
                else
                 add=false;		
                break;				 
			}
		}
		if (add)
         moves.push_back(c);
    }
    void delMove(coord c)
    {
        vector<coord>::iterator it;
        for(it=moves.begin(); it!=moves.end(); it++)
            if (it->i==c.i && it->j==c.j)
            {
                moves.erase(it);
                break;
            }
    }
    void getStreaks(int& maxLine,int who)
    {
        int x,y,v;
	    uint** isChecked=new uint*[n];
		for(int i=0;i<n;i++)
		 isChecked[i]=new uint[m];
		for (int i=0; i<n; i++)
         for(int j=0; j<m; j++)
            isChecked[i][j]=0; 
        for (unsigned int i=0; i<elems.size(); i++)//little bit faster, than with iterator
        {
            x=(elems[i]).i;
            y=(elems[i]).j;
		    coord cells[4];
			if (x!=0)
            {
                cells[1]=coord(x-1,y);
                if(y!=0)
                   cells[3]=coord(x-1,y-1);
                if (y!=m)
                    cells[2]=coord(x-1,y+1);
            }
            if (y!=0)
                cells[0]=coord(x,y-1);
            if ((*field)[x][y]==who)
                for(int g=0; g<4; g++)
                {
				    coord c;
                    v=getLinesAndMoves(who,x,y,g+1,isChecked,c);
					c.weight=v;
					if (c.i!=-1 && c.j!=-1)
					{
					 addMove(c);
					}
					if (cells[g].i!=-1 && cells[g].j!=-1 && (*field)[cells[g].i][cells[g].j]==0)
					{
					 cells[g].weight=v;
					 addMove(cells[g]);
					}
                    if (v>maxLine)
                        maxLine=v;
                }
        }
	    for (int i=0; i<n; i++)
          delete[] isChecked[i];
		delete[] isChecked; 
    }
	int getLinesAndMoves(int who,int i, int j,int d,unsigned int** isChecked,coord& c)
	{
	    int result=0;
        if ((*field)[i][j]==who)
            result=1;
        else
        {
            if (!(*field)[i][j])
             {
			  c.i=i;
			  c.j=j;
			 }
            return result;
        }
		if ((isChecked[i][j]>>d) % 2!=0)//00000xxxx %2
		 return 0;
		else
		 isChecked[i][j]+=(1<<d);//0000xxxx0
		//cout<<"ISCHECKED: "<<isChecked[i][j]<<endl; 
        switch(d)//1: left-right 2: top-bot 3: y=x 4:y=-x
        {
        case 2:
            if(i<n-1)
                result+=getLinesAndMoves(who,i+1,j,d,isChecked,c);
            break;
        case 1:
            if(j<m-1)
                result+=getLinesAndMoves(who,i,j+1,d,isChecked,c);
            break;
        case 3:
            if(i<n-1 && j>0)
                result+=getLinesAndMoves(who,i+1,j-1,d,isChecked,c);
            break;
        case 4:
            if(i<n-1 && j<m-1)
                result+=getLinesAndMoves(who,i+1,j+1,d,isChecked,c);
            break;
        default:
            throw "SOMETHING WENT HORRIBLY WRONG";
        }
        return result;
	}
    int Estimator()//estimating criteria 1)line length 2) line openness 3)TODO: calculating value based on every line
    {
        //int result=0;
        int maxLine[2]= {0,0};
        int maxState[2]= {2,2};
        int currMaxValue[2]= {-2,-2};
        long currValue[2]= {0,0};
        int** f=*field;
        int x,y,v,state,who,t;//state=0 - line unblocked state=1 - line blocked from one side state=2 line blocked
        bool states[4]= {2,2,2,2};
		unsigned int** isChecked=new unsigned int*[n];//maybe it is better to do just bool[4]
		for(int i=0;i<n;i++)
		 isChecked[i]=new unsigned int[m];
		for (int i=0; i<n; i++)
         for(int j=0; j<m; j++)
            isChecked[i][j]=0; 
        for (unsigned int i=0; i<elems.size(); i++)//little bit faster, than with iterator
        {
            x=(elems[i]).i;
            y=(elems[i]).j;
            who=f[x][y];
            states[0]=2;
            states[1]=2;
            states[2]=2;
            states[3]=2;
            if (x!=0)
            {
                states[1]=f[x-1][y];
                if(y!=0)
                    states[3]=f[x-1][y-1];
                if (y!=m)
                    states[2]=f[x-1][y+1];
            }
            if (y!=0)
                states[0]=f[x][y-1];
            for(int g=0; g<4; g++)
            {
                state=2*(int)states[g]+(int)!states[g]%2;
				//cout<<state<<endl;
                v=getLine(who,x,y,g+1,isChecked,state);
				if (v==k)
				 t=k+1;
				else 
                 t=(v/(state/2+1))+1-state;
                //int t=who;
                //currValue[who-1]+=(2<<v)*10+(10-d); 
				if (t>0 && v>1)
				{
				// cout<<"WHO: "<<who<<" v: "<<v<<" state: "<<state<<" g: "<<g+1<<" x: "<<x<<" y: "<<y<<" t: "<<t<<endl;
				 currValue[who-1]+=(2<<t);
				 //currValue[who-1]+=t;
				}
                if (t>currMaxValue[who-1])
                {
                    maxLine[who-1]=v;
                    maxState[who-1]=state;
                    currMaxValue[who-1]=t;
                }
            }
        }
        int result=0;
		if (maxLine[0]>=k)
		 currValue[0]=5000;
		if (maxLine[1]>=k) 
		 currValue[1]=5000;
		currValue[0]=currValue[0]*10+(10-d)*(d%2==0);
		currValue[1]=currValue[1]*10+(10-d)*(d%2==1);
		result = currValue[0]-currValue[1];
		//free resources
        for (int i=0; i<n; i++)
          delete[] isChecked[i];
		delete[] isChecked; 
        return result;
    }

    int getLine(int who,int i, int j,int d,unsigned int** isChecked, int& state)
    {
        int result=0;
        if ((*field)[i][j]==who)
            result=1;
        else
        {
            if (!(*field)[i][j])
                state--;
            return result;
        }
		if ((isChecked[i][j]>>d) % 2!=0)//00000xxxx %2
		 return 0;
		else
		 isChecked[i][j]+=(1<<d);//0000xxxx0
		//cout<<"ISCHECKED: "<<isChecked[i][j]<<endl; 
        switch(d)//1: left-right 2: top-bot 3: y=x 4:y=-x
        {
        case 2:
            if(i<n-1)
                result+=getLine(who,i+1,j,d,isChecked,state);
            break;
        case 1:
            if(j<m-1)
                result+=getLine(who,i,j+1,d,isChecked,state);
            break;
        case 3:
            if(i<n-1 && j>0)
                result+=getLine(who,i+1,j-1,d,isChecked,state);
            break;
        case 4:
            if(i<n-1 && j<m-1)
                result+=getLine(who,i+1,j+1,d,isChecked,state);
            break;
        default:
            throw "SOMETHING WENT HORRIBLY WRONG";
        }
        return result;
    }
    void print()
    {
        for (int j=0; j<m; j++)
        {
            if (j<10)
                cout<<j<<" ";
            else
                cout<<j%10<<" ";
        }
        cout<<endl<<endl;
        for (int i=0; i<n; i++)
        {
            for (int j=0; j<m; j++)
            {
                if ((*field)[i][j]==1)
                    cout<<"O ";
                else if ((*field)[i][j]==2)
                    cout<<"X ";
                else
                    cout<<"# ";
                //cout<<((*field)[i][j])<<" ";
            }
            cout<<" "<<i<<endl;
        }
    }
};
int main()
{
    int** f;
    int n,m,k,d;
    cout<<"Field size N M: ";
    cin>>n>>m;
    cout<<endl;
    cout<<"Win streak and depth of search: ";
    cin>>k>>d;
    cout<<endl;
    f=new int*[n];
    for(int i=0; i<n; i++)
        f[i]=new int[m];
    for (int i=0; i<n; i++)
        for(int j=0; j<m; j++)
            f[i][j]=0;
    Bot cpu(n,m,k,d,&f);
	//int line;
	//cpu.placeMove(7,7,true,line,line);
    while(true)
    {
        int x,y;
        cout<<"YOUR TURN"<<endl;
        cin>>x>>y;
        //f[x][y]=2;
        try
        {
            cpu.MoveMINIMAX(x,y);
        }
        catch(int msg)
        {
            switch(msg)
            {
            case 1:
                cout<<"Cell is already taken, please choose another"<<endl;
                break;
            case 2:
                cout<<"Game Over"<<endl;
                return 0;
                break;
            case 3:
                cout<<"You lost the game"<<endl;
                return 0;
                break;
            case 4:
                cout<<"You won the game"<<endl;
                return 0;
            }
        }
        catch (const char* c)
        {
            cout<<c<<endl;
            return 0;
        }
    }

    return 0;
}

